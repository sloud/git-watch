"use strict";

const fs = require("fs");
const { spawn } = require("child_process");

const gitWatchDir = `${process.env.HOME}/.git-watch`;

// clone the repo if it does not exist
function initRepo(repo) {
  if (!fs.existsSync(repoPath(repo.name))) {
    console.log(`cloning ${repoPath(repo.name)}...`);
    cloneBareRepo(repo);
  }

  return repo;
}

// the path the repo lives at
function repoPath(name) {
  return `${gitWatchDir}/repos/${name}`;
}

function config(name) {
  return require(`${gitWatchDir}/watch/${name}`);
}

function cloneBareRepo(repo) {
  return spawn("git", ["clone", "--bare", repo.remote, repoPath(repo.name)]);
}

// construct branchState objects for each watched branch in a repo
function branchStates(repo) {
  return repo.branches.map((branch) => {
    const state = {
      repo: repo.name,
      branch: branch.name,
      refresh: repo.refresh,
      path: repoPath(repo.name),
      hash: null,
    };

    console.log(state);
    return state;
  });
}

// call git, parse the hash, and update the branch state
function watchBranchState(state) {
  getCurrentCommitHash(state.path, state.branch).on("data", (data) => {
    updateBranchState(state, parseHash(data));
  });
}

// run git to fetch the most recent commit hash from origin for the branch
function getCurrentCommitHash(path, branch) {
  return spawn("git", ["-C", path, "ls-remote", "origin", branch]).stdout;
}

// notify the desktop if the hash changes, and  schedule the next check
function updateBranchState(currentState, hash) {
  const newState = { ...currentState, hash };

  if (currentState.hash && currentState.hash !== hash) {
    const message = notifyMessage(newState.repo, newState.branch)
    
    console.log(message)
    notifySend(message);
  }

  setTimeout(() => {
    watchBranchState(newState);
  }, newState.refresh * 1000);
}

// send a notification to the desktop that the branch has been updated on origin
function notifySend(message) {
  const now = new Date();
  spawn("notify-send", [message.title, message.body]);
}

function notifyMessage(repo, branch) {
  const title = `${repo} (${branch})`;
  const now = new Date();
  const body = `Updated: ${now.toTimeString()}, ${now.toDateString()}`;

  return { title, body };
}

// parse the commit hash from the git command stdout
function parseHash(data) {
  return `${data}`.split(/\s/)[0].trim();
}

// go
function main() {
  fs.readdir(`${gitWatchDir}/watch`, (err, files) => {
    if (err) {
      console.error(err);
      process.exit(1);
    } else {
      files
        .filter((it) => it.endsWith(".json"))
        .map((file) => file.replace(/\.json$/, ""))
        .map(config)
        .map(initRepo)
        .flatMap(branchStates)
        .forEach(watchBranchState);
    }
  });
}

main();
