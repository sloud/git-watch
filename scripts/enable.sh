#!/bin/sh

repo_base="$(git rev-parse --show-toplevel)"

mkdir -p "$HOME/.config/systemd/user/"
cp -v "$repo_base/git-watch.service" "$HOME/.config/systemd/user/"
systemctl --user enable git-watch.service
systemctl --user start git-watch.service

unset repo_base