# git-watch

A small daemon that watches remote repos for changes and generates desktop notifications
when the remote updates.

## Keep in mind

- This is a tool I made for person use, so it works for me. It may not work for you.
- must be installed at `$HOME/.git-watch` to work.
- It has some dependencies.
  - `node`
  - `notify-send` work on a lot of linux distros but not all.

## Todos

- [ ] add time and date to notification message.
- [ ] add `scripts/gen.sh` that will create a json config from a repo on your machine
- [ ] customizable install locations
- [ ] maybe add different notification drivers
  - [ ] macos
  - [ ] windows? (prolly not..)
